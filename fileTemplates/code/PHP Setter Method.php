#set ($hint = ${TYPE_HINT})
#set ($nullable = false)
#set ($firstType = false)
#set ($multiType = false)
#set ($fullType = "")
#foreach( $stringList in $hint.split("[|]") )
    #if ($stringList == "null" || $stringList =="Null")
        #set ($nullable = true)
    #else
        #if(!$firstType)
            #set ($firstType = $stringList)
            #set ($fullType = $stringList)
        #else
            #set ($multiType = true)
            #set ($fullType = $fullType + "|" + $stringList)
        #end
        #if($stringList.contains("Date") || $stringList.contains("Enum"))
            #set($multiType = true)
            #set($fullType = $fullType  + "|string")
        #end
        #if($stringList.contains("mixed"))
            #set($multiType = true)
        #end
    #end
#end
#if ($multiType)
/**
 * @param $fullType#if($nullable)|null#end $${PARAM_NAME}
 */
#end
public ${STATIC} function set${NAME}(#if (!$multiType && $firstType)$firstType #else#end$${PARAM_NAME}#if($nullable) = null#else#end)
{
#if (${STATIC} == "static")
    self::$${FIELD_NAME} = $${PARAM_NAME};
#else
    #if ($hint.contains("Date"))
      $this->${FIELD_NAME} = $firstType::parse($${PARAM_NAME},#if($nullable)true#else false#end);
    #elseif  ($hint.contains("Enum"))
       #if($nullable)
       if($${PARAM_NAME}){
       #end
      $this->${FIELD_NAME} = is_a($${PARAM_NAME},$firstType::class) ? $${PARAM_NAME} : $firstType::memberByValue($${PARAM_NAME});
       #if($nullable)
       }else{
       $this->${FIELD_NAME}=null;
       }
       #end
    #else
      $this->${FIELD_NAME} = $${PARAM_NAME};
    #end
#end
}