<?php
#if (${NAMESPACE})

namespace ${NAMESPACE};

#end
use DigiTickets\Types\AbstractEnumeration;

/**
 * Class ${NAME}
 *
 * @method static ${NAME} ${ENUM_KEY}()
 */
final class ${NAME} extends AbstractEnumeration
{
    const ${ENUM_KEY} = '${ENUM_STRING}';
}
