<?php
#set ($d = "$")
#if (${NAMESPACE})

namespace ${NAMESPACE};

#end
use DigiTickets\Entities\AbstractEntity;
#if (${BranchPropertyTrait}!= '')use DigiTickets\Entities\Traits\BranchPropertyTrait; #end
#if (${CompanyTrait}!= '')use DigiTickets\Entities\Traits\CompanyTrait; #end
#if (${DescriptionPropertyTrait}!= '')use DigiTickets\Entities\Traits\DescriptionPropertyTrait; #end
#if (${DevicePropertyTrait}!= '')use DigiTickets\Entities\Traits\DevicePropertyTrait; #end
#if (${LocationPropertyTrait}!= '')use DigiTickets\Entities\Traits\LocationPropertyTrait; #end
#if (${NamePropertyTrait}!= '')use DigiTickets\Entities\Traits\NamePropertyTrait; #end
#if (${StatusTrait}!= '')use DigiTickets\Entities\Traits\StatusTrait; #end
#if (${TimestampTrait}!= '')use DigiTickets\Entities\Traits\TimestampTrait; #end
#if (${UniqueIntegerTrait}!= '')use DigiTickets\Entities\Traits\UniqueIntegerTrait; #end
#if (${UserCompanyTrait}!= '')use DigiTickets\Entities\Traits\UserCompanyTrait; #end
#if (${UserPropertyTrait}!= '')use DigiTickets\Entities\Traits\UserPropertyTrait; #end
#if (${UserTrait}!= '')use DigiTickets\Entities\Traits\UserTrait; #end

final class ${NAME} extends AbstractEntity
{
    #if (${BranchPropertyTrait}!= '')use BranchPropertyTrait; #end
    #if (${CompanyTrait}!= '')use CompanyTrait; #end
    #if (${DescriptionPropertyTrait}!= '')use DescriptionPropertyTrait; #end
    #if (${DevicePropertyTrait}!= '')use DevicePropertyTrait; #end
    #if (${LocationPropertyTrait}!= '')use LocationPropertyTrait; #end
    #if (${NamePropertyTrait}!= '')use NamePropertyTrait; #end
    #if (${StatusTrait}!= '')use StatusTrait; #end
    #if (${TimestampTrait}!= '')use TimestampTrait; #end
    #if (${UniqueIntegerTrait}!= '')use UniqueIntegerTrait; #end
    #if (${UserCompanyTrait}!= '')use UserCompanyTrait; #end
    #if (${UserPropertyTrait}!= '')use UserPropertyTrait; #end
    #if (${UserTrait}!= '')use UserTrait; #end
    /**
    * @var int|null
    */
    protected ${d}${idField};
    
    public function getID()
    {
        return ${d}this->${idField};
    }
}
