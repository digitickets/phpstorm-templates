<?php
#if (${NAMESPACE})

namespace ${NAMESPACE};

#end
use DigiTickets\TypedArray\AbstractTypedArray;

final class ${NAME} extends AbstractTypedArray
{
    const ARRAY_TYPE =${ENTITY_NAME}::class;
}