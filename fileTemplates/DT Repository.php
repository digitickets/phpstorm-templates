<?php
#set ($d = "$")
#if (${NAMESPACE})

namespace ${NAMESPACE};

#end

use DigiTickets\Entities\AbstractEntityCollection;
use DigiTickets\Pagination\PaginationPosition;
use DigiTickets\Types\Repositories\AbstractRepository;

class ${NAME} extends AbstractRepository
{
    public function getTableName(): string
    {
        return '${TableName}';
    }

    public function getPrimaryKeyName(): string
    {
        return '${PrimaryKey}';
    }

    /**
     * Persisting the ${Entity} entity
     *
     * @param ${Entity} ${d}entity
     *
     * @return ${Entity}
     *
* @throws \Doctrine\DBAL\DBALException
     */
    public function persist(${Entity} ${d}entity): ${Entity}
    {
        return new ${Entity}(parent::persistEntity(${d}entity));
    }

    /**
     * @param int ${d}id
     *
     * @return ${Entity}|null
     */
    public function find(int ${d}id)
    {
        ${d}entityData = parent::findEntityData(${d}id);

        return ${d}entityData ? new ${Entity}(${d}entityData) : null;
    }

    /**
     * @param string|null ${d}filter
     * @param string|null ${d}orderBy
     * @param PaginationPosition|null ${d}paginationPosition
     *
     * @return AbstractEntityCollection
     *
     * @throws \DigiTickets\SPL\Exceptions\InvalidArgumentException
     */
    public function findAll(string ${d}filter = null, string ${d}orderBy = null, PaginationPosition ${d}paginationPosition = null): AbstractEntityCollection
    {
        /** @var ${Entity}Collection ${d}collection */
        ${d}collection = parent::findAllAsCollection(${Entity}Collection::class, ${d}filter, ${d}orderBy, ${d}paginationPosition);

        return ${d}collection;
    }
}
